﻿using System;

namespace Spanglish
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string[] esp;
            string[] E_n;
            string P_1s="", P_2s = "", P_1e="",P_2e="";
            int spañ, engli;
            Console.WriteLine("¿CUANTAS PALABRAS QUIERES EN ESPAÑOL?");
            spañ = Convert.ToInt16(Console.ReadLine());
            esp = new string[spañ];
            Console.WriteLine("¿CUANTAS PALABRAS QUIERES EN INGLES?");
            engli = Convert.ToInt16(Console.ReadLine());
            E_n = new String[engli];
            for (int i = 1; i <= esp.Length; i++)
            {
                Console.WriteLine("INGRESA LA PALABRA {0} EN ESPAÑOL: ",(i));
                esp[i-1] = Console.ReadLine();
            }
            for (int i = 1; i <= E_n.Length; i++)
            {
                Console.WriteLine("INGRESA LA PALABRA {0} EN INGLES: ",(i));
                E_n[i - 1] = Console.ReadLine();
            }
            for (int i = 0; i < E_n.Length; i++)
            {
                P_1s = esp[i].Substring(startIndex:0,length:(esp[i].Length/2));
                P_2s = esp[i].Substring(startIndex:(esp[i].Length/2),length:((esp[i].Length) - (esp[i].Length/2)));
                P_1e = E_n[i].Substring(startIndex:0,length:(E_n[i].Length/2));
                P_2e = E_n[i].Substring(startIndex:(E_n[i].Length/2),length:((E_n[i].Length) - (E_n[i].Length/2)));
                Console.WriteLine(" 1 en ESPAÑOL con  2 en INGLES: {0}",(P_1s + P_2e));
                Console.WriteLine(" 1 en INGLES con   2 en ESPAÑOL: {0}",(P_1e + P_2s));
                Console.WriteLine(" 2 en INGLES con   1 en ESPAÑOL: {0}",(P_2e + P_1s));
                Console.WriteLine(" 2 en ESPAÑOL con  1 en INGLES: {0}",(P_2s + P_1e));
            }
        }
    }  
        }
    
